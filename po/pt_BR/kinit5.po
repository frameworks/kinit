# Translation of kinit5.po to Brazilian Portuguese
# Copyright (C) 2002-2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Lisiane Sztoltz <lisiane@conectiva.com.br>, 2002, 2003, 2004.
# Lisiane Sztoltz Teixeira <lisiane@kdemail.net>, 2003, 2004, 2005.
# Henrique Pinto <stampede@coltec.ufmg.br>, 2003.
# Marcus Gama <marcus.gama@gmail.com>, 2006.
# Diniz Bortolotto <diniz.bortolotto@gmail.com>, 2007, 2008.
# André Marcelo Alvarenga <alvarenga@kde.org>, 2008, 2009, 2010, 2011, 2012, 2013, 2014.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2008, 2009, 2010, 2012, 2017.
# Fernando Boaglio <boaglio@kde.org>, 2009.
# Doutor Zero <doutor.zero@gmail.com>, 2007, 2009.
# Marcus Vinícius de Andrade Gama <marcus.gama@gmail.com>, 2010, 2012.
# Aracele Torres <araceletorres@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kinit5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-01 21:29+0000\n"
"PO-Revision-Date: 2017-12-12 15:46-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"

#: kdeinit/kinit.cpp:448
#, kde-format
msgid ""
"Unable to start new process.\n"
"The system may have reached the maximum number of open files possible or the "
"maximum number of open files that you are allowed to use has been reached."
msgstr ""
"Não foi possível iniciar o novo processo.\n"
"O sistema pode ter alcançado o número máximo possível de arquivos abertos ou "
"foi alcançado o número máximo de arquivos abertos que é permitido a você "
"usar."

#: kdeinit/kinit.cpp:470
#, kde-format
msgid ""
"Unable to create new process.\n"
"The system may have reached the maximum number of processes possible or the "
"maximum number of processes that you are allowed to use has been reached."
msgstr ""
"Não foi possível criar o novo processo.\n"
"O sistema pode ter alcançado o número máximo possível de processos ou foi "
"alcançado o número máximo de processos que é permitido a você usar."

#: klauncher/klauncher.cpp:418
#, kde-format
msgid "KDEInit could not launch '%1'"
msgstr "O KDEInit não pôde iniciar '%1'"

#: klauncher/klauncher.cpp:594 klauncher/klauncher.cpp:608
#, kde-format
msgid "Could not find service '%1'."
msgstr "Não foi possível encontrar o serviço '%1'."

#: klauncher/klauncher.cpp:626
#, kde-format
msgid "Service '%1' must be executable to run."
msgstr "O serviço '%1' deve ser executável para executar."

#: klauncher/klauncher.cpp:628 klauncher/klauncher.cpp:689
#, kde-format
msgid "Service '%1' is malformatted."
msgstr "O serviço '%1' está mal-formatado."

#: klauncher/klauncher.cpp:784
#, kde-format
msgid "Launching %1"
msgstr "Lançando %1"

#: klauncher/klauncher.cpp:965
#, kde-format
msgid "Unknown protocol '%1'.\n"
msgstr "Protocolo desconhecido '%1'.\n"

#: klauncher/klauncher.cpp:971
#, kde-format
msgid "Could not find the '%1' plugin.\n"
msgstr "Não foi possível encontrar o plugin '%1'.\n"

#: klauncher/klauncher.cpp:1044
#, kde-format
msgid "Error loading '%1'."
msgstr "Erro ao carregar '%1'."

#: klauncher/klauncher_main.cpp:136
#, kde-format
msgid ""
"klauncher: This program is not supposed to be started manually.\n"
"klauncher: It is started automatically by kdeinit5.\n"
msgstr ""
"klauncher: Este programa não é para ser iniciado manualmente.\n"
"klauncher: Ele é iniciado automaticamente pelo kdeinit5.\n"

#~ msgid "Could not find '%1' executable."
#~ msgstr "Não foi possível encontrar o executável '%1'."

#~ msgid ""
#~ "Could not open library '%1'.\n"
#~ "%2"
#~ msgstr ""
#~ "Não foi possível abrir a biblioteca '%1'.\n"
#~ "%2"

#~ msgid ""
#~ "Could not find 'kdemain' in '%1'.\n"
#~ "%2"
#~ msgstr ""
#~ "Não foi possível encontrar 'kdemain' em '%1'.\n"
#~ "%2"
