# translation of kdelibs4.po to Lithuanian
# Ričardas Čepas <rch@richard.eu.org>, 2002-2004.
# Donatas Glodenis <dgvirtual@akl.lt>, 2004-2009.
# Gintautas Miselis <gintautas@miselis.lt>, 2008.
# Andrius Štikonas <andrius@stikonas.eu>, 2009.
# Tomas Straupis <tomasstraupis@gmail.com>, 2011.
# Remigijus Jarmalavičius <remigijus@jarmalavicius.lt>, 2011.
# Liudas Ališauskas <liudas.alisauskas@gmail.com>, 2011, 2012, 2013, 2014.
# Liudas Alisauskas <liudas@akmc.lt>, 2013, 2015.
msgid ""
msgstr ""
"Project-Id-Version: kdelibs4\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-01 21:29+0000\n"
"PO-Revision-Date: 2019-09-07 13:52+0300\n"
"Last-Translator: Moo\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 2.2.1\n"

#: kdeinit/kinit.cpp:448
#, kde-format
msgid ""
"Unable to start new process.\n"
"The system may have reached the maximum number of open files possible or the "
"maximum number of open files that you are allowed to use has been reached."
msgstr ""
"Nepavyko pradėti naujo proceso.\n"
"Sistema pasiekė maksimalų galimų atverti failų skaičių, arba jūs pasiekėte "
"maksimalų jums leidžiamą galimų atverti failų skaičių."

#: kdeinit/kinit.cpp:470
#, kde-format
msgid ""
"Unable to create new process.\n"
"The system may have reached the maximum number of processes possible or the "
"maximum number of processes that you are allowed to use has been reached."
msgstr ""
"Nepavyko sukurti naujo proceso.\n"
"Sistema pasiekė maksimalų galimų procesų skaičių, arba jūs pasiekėte "
"maksimalų jums leidžiamą procesų skaičių."

#: klauncher/klauncher.cpp:418
#, kde-format
msgid "KDEInit could not launch '%1'"
msgstr "KDEInit nesugebėjo paleisti „%1“"

#: klauncher/klauncher.cpp:594 klauncher/klauncher.cpp:608
#, kde-format
msgid "Could not find service '%1'."
msgstr "Nepavyko rasti tarnybos „%1“."

#: klauncher/klauncher.cpp:626
#, kde-format
msgid "Service '%1' must be executable to run."
msgstr "Tarnyba „%1“ privalo būti vykdomoji, norint ją paleisti."

#: klauncher/klauncher.cpp:628 klauncher/klauncher.cpp:689
#, kde-format
msgid "Service '%1' is malformatted."
msgstr "Tarnyba „%1“ yra blogai suformuota."

#: klauncher/klauncher.cpp:784
#, kde-format
msgid "Launching %1"
msgstr "Paleidžiama %1"

#: klauncher/klauncher.cpp:965
#, kde-format
msgid "Unknown protocol '%1'.\n"
msgstr "Nežinomas protokolas \"%1\".\n"

#: klauncher/klauncher.cpp:971
#, kde-format
msgid "Could not find the '%1' plugin.\n"
msgstr "Nepavyko rasti papildinio \"%1\".\n"

#: klauncher/klauncher.cpp:1044
#, kde-format
msgid "Error loading '%1'."
msgstr "Klaida įkeliant „%1“."

#: klauncher/klauncher_main.cpp:136
#, kde-format
msgid ""
"klauncher: This program is not supposed to be started manually.\n"
"klauncher: It is started automatically by kdeinit5.\n"
msgstr ""
"klauncher: Ši programa neturėtų būti paleidžiama rankiniu būdu.\n"
"klauncher: Ją automatiškai paleidžia kdeinit5.\n"

#~ msgid "Could not find '%1' executable."
#~ msgstr "Nepavyko rasti „%1“ vykdomojo failo."

#~ msgid ""
#~ "Could not open library '%1'.\n"
#~ "%2"
#~ msgstr ""
#~ "Nepavyko atverti bibliotekos „%1“.\n"
#~ "%2"

#~ msgid ""
#~ "Could not find 'kdemain' in '%1'.\n"
#~ "%2"
#~ msgstr ""
#~ "Nepavyko rasti „kdemain“ „%1“ faile.\n"
#~ "%2"
